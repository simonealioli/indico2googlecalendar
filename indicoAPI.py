# -*- coding: utf-8; show-trailing-whitespace: t; truncate-lines: t; -*-
#
# Copyright (C) 2013 Marco Rovere
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import hashlib
import hmac
import urllib, urllib2
import time
import sys
import cjson
import re
from subprocess import call
from datetime import datetime as dt
from configobj import ConfigObj


class IndicoAPI:
    SERVER = 'https://indico.cern.ch'
    PATH   =  '/export/categ/'

    def __init__(self, query, format):
        self._query = query
        self._format = format
        config = ConfigObj('i-go.cfg')
        self._api_key = config['IndicoSecrets']['api_key']
        self._secret_key = config['IndicoSecrets']['secret_key']

    def request(self,
                only_public=False,
                persistent=False):
        items = self._query['params'].items() if hasattr(self._query['params'], 'items') else list(self._query['params'])
        path = self.PATH + '%s.%s' % (self._query['cat'], self._format)
        if self._api_key:
            items.append(('ak', self._api_key))
        if only_public:
            items.append(('onlypublic', 'yes'))
        if self._secret_key:
            if not persistent:
                items.append(('timestamp', str(int(time.time()))))
            items = sorted(items, key=lambda x: x[0].lower())
            url = '%s?%s' % (path, urllib.urlencode(items))
            signature = hmac.new(self._secret_key, url, hashlib.sha1).hexdigest()
            items.append(('signature', signature))
        if not items:
            return path
        url = "%s%s?%s" % (self.SERVER, path, urllib.urlencode(items))
        result = urllib2.urlopen(url)
        if result.headers.get('Content-type', '') == 'application/json':
            return cjson.decode(result.read())
        else:
            print 'Format not supported'
            print result.read()

    def indico_to_google_format(self,
                                indico_ev,
                                calendar_id_sha,
                                cat_id,
                                contr_id = 0):
      event = {}
      speaker = indico_ev['chairs'][0]
      event['summary'] = speaker['first_name']+" "+ speaker['last_name']+ " ("+speaker['affiliation']+") - "+indico_ev["category"]
      event['description'] = "Title:\n"+indico_ev['title'] + "\n\nAbstract:\n"+ re.sub('<(.*?)>', '', indico_ev['description'].replace('\\',''))+"\n\nMore info and slides at "+indico_ev['url'].replace('\\','')
      event['location'] = '%s - %s' % (indico_ev['location'],
                                       indico_ev['room'])
      event['start'] = {}
      event['start']['timeZone'] = indico_ev['startDate']['tz'].replace('\\','')
      event['start']['dateTime'] = '%sT%s' % (indico_ev['startDate']['date'],
                                              indico_ev['startDate']['time'])
      event['end'] = {}
      event['end']['timeZone'] = indico_ev['endDate']['tz'].replace('\\','')
      event['end']['dateTime'] = '%sT%s' % (indico_ev['endDate']['date'],
                                            indico_ev['endDate']['time'])
      event['source'] = {}
      event['source']['title'] = indico_ev['title']
      event['source']['url'] = indico_ev['url'].replace('\\','')
      event['iCalUID'] = 'indico-event-%s_ctr_id%s_gcid_%s@cern.ch' % (cat_id,
                                                                       contr_id,
                                                                       calendar_id_sha)

      event['reminders'] = {}
      event['reminders']['useDefault'] = False
      event['reminders']['overrides'] = []
      # event['reminders']['overrides'].append({'method': 'email',
      #                                        'minutes': 24*60})
      event['reminders']['overrides'].append({'method': 'email',
                                              'minutes': 60})
      # event['reminders']['overrides'].append({'method': 'sms',
      #                                         'minutes': 60})
      return event


if __name__ == '__main__':
    category = []
    category.append({'Name': 'Upgrade',
                     'cat': 5353,
                     'params': {'limit': 100,
                                'from':  dt.now().strftime('%Y-%m-%d'),
                                'pretty': True,
                                'detail': 'contributions'}})

    for c in category:
        indico_api = IndicoAPI(c, 'json')
        results = indico_api.request()
        for r in  results['results']:
            if c['params'].get('detail', None):
                if len(r['contributions']):
                    for contr in r['contributions']:
                        print contr
