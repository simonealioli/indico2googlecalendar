#!/usr/bin/python2

# -*- coding: utf-8; show-trailing-whitespace: t; truncate-lines: t; -*-
#
# Copyright (C) 2013 Marco Rovere
# Copyright (C) 2013 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Command-line skeleton application for the I-Go calendar API.
Usage:
  $ python i-go.py

You can also get help on all the command-line flags the program understands
by running:

  $ python i-go.py --help

"""

import argparse
import httplib2
import os
import sys
import re
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from urllib import quote
from indicoAPI import *
from configobj import ConfigObj
from hashlib import sha256 as sha

from apiclient import discovery
from oauth2client import file
from oauth2client import client
from oauth2client import tools

# Parser for command-line arguments.
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[tools.argparser])

parser.add_argument('-c', '--category',
                    type=int,
                    help='Consider only event belonging to the specified category.')
parser.add_argument('-f', '--force-past-update',
	            action='store_true',
                    default=False,
                    help='Try to force events that are in the past with respect to the runtime.')

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')
FLOW = client.flow_from_clientsecrets(CLIENT_SECRETS,
  scope=[
      'https://www.googleapis.com/auth/calendar',
      'https://www.googleapis.com/auth/calendar.readonly',
    ],
    message=tools.message_if_missing(CLIENT_SECRETS))

def check_if_updated(indico_event, google_event):
  blacklist = ['reminders']
  updated = False
  for k in indico_event.keys():
    # Skip reminders since they could be ordered differently
    if k in blacklist:
      continue
    if not indico_event[k] == google_event[k]:
      updated = True
      google_event[k] = indico_event[k]

  return (updated, google_event)

def main(argv):
  # Parse the command-line flags.
  flags = parser.parse_args(argv[1:])
  config = ConfigObj('i-go.cfg')

  # If the credentials don't exist or are invalid run through the native client
  # flow. The Storage object will ensure that if successful the good
  # credentials will get written back to the file.
  storage = file.Storage('sample.dat')
  credentials = storage.get()
  if credentials is None or credentials.invalid:
    credentials = tools.run_flow(FLOW, storage, flags)

  # Create an httplib2.Http object to handle our HTTP requests and authorize it
  # with our good Credentials.
  http = httplib2.Http()
  http = credentials.authorize(http)

  # Construct the service object for the interacting with the Calendar API.
  service = discovery.build('calendar', 'v3', http=http)

  cfg = config.dict()
  # The sha of the calendar id is used to 'localize' each event to a
  # specific calendar, so that the user can add the same events to
  # multiple calendars avoid conflicts in the unique event's id.
  google_calendar_id = cfg['GoogleCalendar']['id']
  google_calendar_sha = sha(google_calendar_id).hexdigest()
  indico_cfg = cfg['IndicoCalendars']

  # Get events already present in Google Calendar.
  google_events = None
  page_token = None
  try:
    while True:
      google_events = service.events().list(calendarId = google_calendar_id,
                                            timeMin = (dt.now()+relativedelta(months=-2)).strftime('%Y-%m-%dT00:00:01Z'),
                                            showDeleted = True,
                                            pageToken = page_token).execute()
      page_token = google_events.get('nextPageToken')
      if not page_token:
        break
  except client.AccessTokenRefreshError:
    print ("The credentials have been revoked or expired, please re-run"
           "the application to re-authorize")

  for c in indico_cfg.keys():
    if flags.category and flags.category != int(indico_cfg[c]['cat']):
      print("Ignoring category %s" % indico_cfg[c]['cat'])
      continue
    indico_events = []
    print "Querying %s [cat_id %s]" % (indico_cfg[c]['name'], indico_cfg[c]['cat'])
    # Get user's selected events from indico
    indico_api = IndicoAPI(indico_cfg[c], 'json')
    events = indico_api.request()
    if events['count'] > 0:
      print 'Found %d event(s)' % events['count']
      for ev in events['results']:
        if not ev['chairs']:
          print 'Skipping %s due to missing author info'
        else:
          print 'Studying ... %s - %s' % (ev['chairs'][0]['fullName'],ev['title'])
          if indico_cfg[c]['params'].get('detail', None):
            if len(ev['contributions']) > 0:
              # Indico currently does not apply the 'from' filtering to
              # the contribution list, so that we can happen to receive
              # events in the past if they belong to a contribution that
              # extends in the future. Do a hard-coded check here to
              # discard such events.
              for contr in ev['contributions']:
                try:
                  if dt.strptime('%sT%s' % (contr['startDate']['date'],
                                          contr['startDate']['time']),
                               '%Y-%m-%dT%H:%M:%S') < dt.now() and not flags.force_past_update:
                    print 'Skipping past contribution ... "%s"' % contr['title']
                    continue
                  indico_events.append(
                    indico_api.indico_to_google_format(contr,
                                                     google_calendar_sha,
                                                     ev['id'], contr['id']))
                except:
                  print "skipping malformed event: ", contr
          else:
            indico_events.append(
              indico_api.indico_to_google_format(ev,
                                               google_calendar_sha,
                                               ev['id']))
    for indico_e in indico_events:
      new_event = True
      for google_e in google_events['items']:
        if google_e['iCalUID'] == indico_e['iCalUID']:
          new_event = False
          (updated, new_google_e) = check_if_updated(indico_e, google_e)
          if updated:
            new_google_e['sequence'] = google_e['sequence'] + 1
            new_google_e['status'] = "confirmed"
            print 'Event "%s" already existing ... updating' % new_google_e['summary']
            try:
               imported_event = service.events().update(calendarId = google_calendar_id,
                                                        eventId = new_google_e['id'],
                                                        body = new_google_e).execute()
            except Exception as e:
               print "ERROR : %s" % e
               pass
          else:
            print "Event NOT updated"
          continue;
      if new_event:
        print 'New Event %s, creating ...' % indico_e['summary']
        try:
           created_event = service.events().insert(calendarId = google_calendar_id,
                                                   body=indico_e).execute()
        except Exception as e:
           print "ERROR : %s" % e
           pass

# For more information on the Calendar API you can visit:
#
#   https://developers.google.com/google-apps/calendar/firstapp
#
# For more information on the Calendar API Python library surface you
# can visit:
#
#   https://developers.google.com/resources/api-libraries/documentation/calendar/v3/python/latest/
#
# For information on the Python Client Library visit:
#
#   https://developers.google.com/api-client-library/python/start/get_started
if __name__ == '__main__':
  main(sys.argv)
