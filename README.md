# Introduction

**I-Go** is a very simple, preliminary script to automatically
transfer events/contributions from indico into a Google Calendar.

You can run in from Mac or Linux.

# Requirements

## Software Requirements

You should use any recent version of Python > 2.6.

There are a few mandatory packages that you need to have installed on
your machine before running the program. Installing them is totally
trivial.

* Virtualenv

With virtualenv you can install the mandatory packages "locally" and
not globally, so that you do not even need root privileges.
```
sudo pip install virtualenv
```
With virtualenv installed, you cna setup a virtualenv to use with the
i-go.py script:
```
virtualenv I-Go_venv
source I-Go_venv/bin/activate
```

* Google python API client

```
easy_install --upgrade google-api-python-client
```

* Smart configuration file parser

```
easy_install configobj
```

* CJson

```
easy_install python-cjson
```

## Authentication Requirements

### Indico authentication

In order to be able to query the indico server via API, you need to
create an API key. In order to do that you need to:

1. connect and login into [indico](http://indico.cern.ch)

1. click on your username at the top-right corner of the page

1. go to *My Profile \\(\Rightarrow\\) HTTP API*

1. click the *Create API key* button.

This will create an API Key and a Secret Key. You should than paste
these quantities into the **IndicoSecrets** section of the
configuration file `i-go.cfg`, in the corresponsing fields.

### Google authentication

**I-Go** **should** transparently take care of authentication. The
first time you run the application you should be redirected to a web
page that will ask you to grant permission to I-Go to access and
modify your calendars. If you trust the application, grant permission.
**A file is locally stored in the folder from within which you run the
application. The file contains your token to allow the application to
use your calendar: you should not put it in shared folders.**

## Configuration

The configuration file `i-go.cfg` contains all the information that
will be transferred from indico into the Google Calendar you want. The
configuration file is divided into sections.

### GoogleCalendar

Here you have to specify the id of the Google Calendar into which you
want to store the events. **I strongly recommend to create a
temporary/fake calendar in Google and use that one to play a bit with
the application, w/o jeopardizing your default calendars.**

To get the ID of a calendar you need to click on the little arrow
besides the calendar name and select *Calendar Settings*. You will be
redirect to another page that contains a **Calendar Address:** field:
you need to copy&paste the calendar ID shown here into the id field of
GoogleCalendar section.

### IndicoSecrets

This section has been already explained in the **Indico
authentication** section of the documentation.

### IndicoCalendars

The section should be easily understandable. Add one entry for each
category you would like to transfer into your preferred Google
Calendar. **NOTA BENE: some meetings are strangely booked in indico,
i.e., they are booked a single event that spans the full week, divided
into contributions that are localised in specific days of the week. In
this case transferring directly the event does not make sense, since
you will end up having a single week-long event w/o any useful
information. In these cases you can use the** `'detail':
'contributions'` **option into the** `'paramas'` **subsection, to tell
the application that you really want to transfer each individual
contribution, not the generic full week event.**

## Running the script

You can grab the sources from this repo.

Running the script is as easy as:

```
python i-go.py
```

You should see in the terminal a long and detailed debug list of what
is happening under the hood.

# FEEDBACK VERY WELCOME

